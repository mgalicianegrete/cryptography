import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyAgreement;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Diffie-Hellman at AES
 * Created by Macario Galicia Negrete
 */
public class ECDH_AES {

    private static final String AES = "AES/ECB/PKCS5Padding";

    private static final int AES_KEY_SIZE = 256;

    public static KeyPair genDHKeyPair() throws NoSuchAlgorithmException {
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("DH");
        kpg.initialize(2048);
        return kpg.genKeyPair();
    }

    public static byte[] encrypt(SecretKey key, String algorithm, byte[] toEncrypt) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return cipher.doFinal(toEncrypt);
    }

    public static byte[] decrypt(SecretKey key, String algorithm, byte[] toDecrypt) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.DECRYPT_MODE, key);
        return cipher.doFinal(toDecrypt);
    }

    public static SecretKey agreeSecretKey(PrivateKey privateKey, PublicKey publicKey, String algoritm, int size, boolean lastPhase) throws Exception {
        // Instantiates and inits a KeyAgreement
        KeyAgreement keyAgreement = KeyAgreement.getInstance("DH");
        keyAgreement.init(privateKey);
        keyAgreement.doPhase(publicKey, lastPhase);
        // Generates the shared secret
        byte[] aux = keyAgreement.generateSecret();
        char[] secret = Base64.getEncoder().encodeToString(aux).toCharArray();
        byte[] salt = "123456789987456321".getBytes();
        

        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        KeySpec spec = new PBEKeySpec(secret, salt, 65536, size);
        SecretKey tmp = factory.generateSecret(spec);
        byte[] encoded = tmp.getEncoded();
        return new SecretKeySpec(encoded, algoritm);
    }
    public static void main(String[] args) throws Exception {
        // Generates keyPairs for Alice and Bob
        final KeyPair kp1 = genDHKeyPair();
        final KeyPair kp2 = genDHKeyPair();
        // Gets the public key of Alice(g^X mod p) and Bob (g^Y mod p)
        final PublicKey pbk1 = kp1.getPublic();
        final PublicKey pbk2 = kp2.getPublic();
        // Gets the private key of Alice X and Bob Y
        PrivateKey prk1 = kp1.getPrivate();
        PrivateKey prk2 = kp2.getPrivate();
        // Computes secret keys for Alice (g^Y mod p)^X mod p == Bob (g^X mod p)^Y mod p
        SecretKey key1 = agreeSecretKey(prk1, pbk2, "AES", AES_KEY_SIZE, true);
        SecretKey key2 = agreeSecretKey(prk2, pbk1, "AES", AES_KEY_SIZE, true);
        // Init the cipher with Alice's key1
        String text = "Hi Bob, how are you?";
        byte[] cipherText = encrypt(key1, AES, text.getBytes());
        // Init the decipher with Bob's key2
        byte[] decipherText = decrypt(key2, AES, cipherText);

        System.out.println("Algorithm: " + AES);
        System.out.println("Encrypted: " + Base64.getEncoder().encodeToString(cipherText));
        System.out.println("Decrypted: " + new String(decipherText, StandardCharsets.UTF_8));
        
    }
    
}
